<?php

function view($template, $data = [])
{
	extract($data);

	return require "app/resources/views/{$template}.view.php";
}

// Application-wide exception handler
function exception_handler($exception)
{
	//if env: production -> write exceptions to log files
	// else:
	echo "Uncaught exception: " , $exception->getMessage(), "\n";
}

