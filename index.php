<?php
// Ei tule käyttää tuotannossa!
error_reporting(E_ALL);
ini_set('display_errors', 1);

session_start();

require "vendor/autoload.php";
require "Core/bootstrap.php";

set_exception_handler('exception_handler');

use App\Core\Router;
use App\Core\App;

Router::define("app/routes.php")
	->fire(
		App::get('request')->getPathInfo(),
		App::get('request')->getMethod()
	);

