<?php
namespace App\Models;

class Task extends Model
{
	protected static $tableName = 'todo';

	public $description;
	public $completed;
	public $id;
}