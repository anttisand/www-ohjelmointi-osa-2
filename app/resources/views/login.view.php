<?php require "_header.view.php"; ?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Log in</h3>
		</div>
		<div class="panel-body">
			<?php if(isset($message)): ?>
				<div class="alert alert-danger" role="alert"><?= $message; ?></div>
			<?php endif; ?>
			<form action="/login" method="POST">
				<div class="form-group">
					<label for="email">Email</label>
					<input type="text" name="email" class="form-control" id="email" placeholder="Enter your email">
				</div>

				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" name="password" class="form-control" id="password">
				</div>
				<button type="submit" class="btn btn-primary">Log in</button>
			</form>
		</div>
	</div>
<?php require "_footer.view.php"; ?>