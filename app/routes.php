<?php

$router->get('/', 'HomeController@index');
$router->get('/about', 'HomeController@about');
$router->get('/todos', 'TodosController@index');
$router->get('/newtask', 'TodosController@create');
$router->post('/todos', 'TodosController@save');

$router->get('/register', 'UsersController@create');
$router->post('/register', 'UsersController@save');
$router->get('/login', 'UsersController@showLogin');
$router->post('/login', 'UsersController@login');
$router->get('/logout', 'UsersController@logout');

