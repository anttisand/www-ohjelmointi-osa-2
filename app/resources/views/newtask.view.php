<?php require "_header.view.php"; ?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Add a new task</h3>
		</div>
		<div class="panel-body">
			<form action="/todos" method="POST">
				<div class="form-group">
					<label for="desc">Task description</label>
					<input type="text" name="description" class="form-control" id="desc" placeholder="Enter task description">
				</div>
				<button type="submit" class="btn btn-primary">Add task</button>
			</form>
		</div>
	</div>
<?php require "_footer.view.php"; ?>