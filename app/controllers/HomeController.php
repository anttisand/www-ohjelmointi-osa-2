<?php
namespace App\Controllers;

class HomeController
{
	public function index()
	{
		$message = 'Hello World!';
		return view("index", compact("message"));
	}

	public function about()
	{
		$message = 'Hello from about page';
		return view("about", compact("message"));
	}
}
