<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Micro MVC</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="container">

	<nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">App name</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
				<li><a href="/">Home</a></li>
				<li><a href="/about">About</a></li>
				<li><a href="/todos">Todos list</a></li>
				<?php if(isset($_SESSION['name'])): ?>
					<li><a href="/logout">Logout (<?= $_SESSION['name']; ?>)</a></li>
				<?php else: ?>
					<li><a href="/register">Register</a></li>
					<li><a href="/login">Login</a></li>
				<?php endif; ?>
            </ul>
          </div>
        </div>
      </nav>
