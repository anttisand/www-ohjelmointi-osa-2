<?php
namespace App\Controllers;

use App\Core\App;
use App\Models\Task;

class TodosController
{
	public function index()
	{
		$tasks = Task::all();

		return view('tasks', compact("tasks"));
	}

	public function create()
	{
		if(!isset($_SESSION['name'])) {
			return view("login", ["message" => "Please log in to do that!"]);
		}

		return view("newtask");
	}

	public function save()
	{
		if(!isset($_SESSION['name'])) {
			return view("login", ["message" => "Please log in to do that!"]);
		}

		$request = App::get('request');
		$id = Task::create([
			'description' => $request->get('description'),
			'completed' => false
		]);

		header('Location: /index.php/todos');
	}
}




