<?php require "_header.view.php"; ?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Tasks list</h3>
		</div>
		<div class="panel-body">
			<div class="list-group">
				<li class="list-group-item active">Tasks list</li>
				<?php foreach($tasks as $task): ?>
					<li class="list-group-item"><?= $task->description; ?></li>
				<?php endforeach; ?>
			</div>
		</div>
	</div>

	<?php if(isset($_SESSION['name'])): ?>
		<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Create a new task, <?= $_SESSION['name']; ?>?</h3>
		</div>
		<div class="panel-body">
			<p><a class="btn btn-success" href="newtask">Lisää uusi taski</a></p>
		</div>
	</div>

	<?php endif; ?>
<?php require "_footer.view.php"; ?>